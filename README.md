# TMap

#### 介绍
layui+腾讯地图坐标点选取插件，支持地址关键字模糊/联想搜索、当前城市定位、地址地图标点联动

#### 使用教程
请clone到本地后直接查看使用教程

#### 在线体验地址
http://tmap.tifeme.com/

#### 插件截图
![定位当前城市](https://images.gitee.com/uploads/images/2020/0103/204839_38dc000f_1368585.png "15241968_1578045780054_25617.png")
![关键字联想搜索](https://images.gitee.com/uploads/images/2020/0103/204925_570c3c9e_1368585.png "15241968_1578045808855_83724.png")
![地址联动地图标注](https://images.gitee.com/uploads/images/2020/0103/204942_0fcbdbe5_1368585.png "15241968_1578045830877_64616.png")
